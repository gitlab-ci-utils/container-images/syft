# Changelog

## v1.2.8 (2025-02-18)

### Fixed

- Updated base image to Alpine 3.21.3.

## v1.2.7 (2025-01-08)

### Fixed

- Updated base image to Alpine 3.21.2.

## v1.2.6 (2025-01-07)

### Fixed

- Updated base image to Alpine 3.21.1.

## v1.2.5 (2024-12-13)

### Fixed

- Updated base image to Alpine 3.21.0.

## v1.2.4 (2024-09-24)

### Fixed

- Fixed container image OCI annotations to match project.

## v1.2.3 (2024-09-06)

### Fixed

- Updated base image to Alpine 3.20.3.

## v1.2.2 (2024-07-23)

### Fixed

- Updated base image to Alpine 3.20.2.

## v1.2.1 (2024-06-23)

### Fixed

- Updated base image to Alpine 3.20.1.

## v1.2.0 (2024-05-22)

### Changed

- Updated image to Alpine 3.20.

## v1.1.1 (2024-04-19)

### Fixed

- Fixed issue with the `amd64` container image being built with the `arm64`
  Syft binary. The Dockerfile and build have been updated to retrieve Syft
  binary directly from source repository to avoid any confusion. (#4)
  - In some cases, for reasons that are unclear, Syft was still executing and
    producing a valid SBOM.

Note: **All** previous image tags were erroneously built with the incorrect
(`arm64`) Syft binary. Image tags for Syft `v1.0.0` and later have been
re-built with the correct binaries, but will have different digests. If any
previous image tags are needed, please open an issue.

### Miscellaneous

- Updated Renovate config to use use new
  [presets](https://gitlab.com/gitlab-ci-utils/renovate-config) project. (#3)

## v1.1.0 (2023-12-09)

### Changed

- Updated image to Alpine 3.19.

## v1.0.0 (2023-10-09)

Initial release
