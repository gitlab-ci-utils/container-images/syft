FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

ARG VERSION

WORKDIR /syft

RUN wget -q -O /tmp/syft.tar.gz https://github.com/anchore/syft/releases/download/v${VERSION}/syft_${VERSION}_linux_amd64.tar.gz && \
  tar -xzf /tmp/syft.tar.gz -C /syft/ && \
  rm -f /tmp/syft.tar.gz

ENTRYPOINT ["/syft/syft"]
CMD [".", "-o", "cyclonedx-json=syft.cdx.json"]

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/container-images/syft"
LABEL org.opencontainers.image.title="syft"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/container-images/syft"
