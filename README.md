# Syft

An Alpine-based container image to run [Syft](https://github.com/anchore/syft)
to generate Software Bill of Materials (SBOMs). This provides a shell, which
is required for use in GitLab CI (whereas the
[original Syft image](https://hub.docker.com/r/anchore/syft) has no shell).

## Install

`$ docker pull registry.gitlab.com/gitlab-ci-utils/container-images/syft:latest`

All available container image tags can be found in the
`gitlab-ci-utils/container-images/syft` repository at
<https://gitlab.com/gitlab-ci-utils/container-images/syft/container_registry>
and are built only for `linux/amd64` (OS/architecture). The following container
image tags are available:

- `vX.Y.Z`: Tags for specific Syft versions (for example `v1.2.0`)
- `latest`: Tag corresponding to the latest commit to the `main` branch of
  this repository

Tags for Syft releases are available for at least a year. All other image tags
may be removed at any time to manage storage.

**Note:** all other image tags in this repository, and any images in the
`gitlab-ci-utils/container-images/syft/tmp` repository, are temporary images
used during the build process and may be deleted at any point.

## Usage

### GitLab CI example

The following is an example job from a `.gitlab-ci.yml` file to use this image
to generate an SBOM with Syft.

```yml
syft_sbom:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/container-images/syft:latest
    entrypoint: [""]
  stage: test
  needs: []
  script:
    - /syft/syft $CI_PROJECT_DIR -o cyclonedx-json=syft.cdx.json
  artifacts:
    paths:
      - syft.cdx.json
    reports:
      cyclonedx:
        - syft.cdx.json
```
